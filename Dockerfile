#getting base image from ubuntu
FROM node:latest

MAINTAINER dakota brinson <brinson6@live.marshall.edu>

WORKDIR /usr/src/engineering-server

COPY ./Engineering-Server/package*.json ./

RUN apt-get update

# imported from FROM node:latest
#RUN apt-get -y install nodejs

RUN npm install

COPY ./Engineering-Server ./

EXPOSE 8080
EXPOSE 3306

CMD ["node", "firstoff.js"]
