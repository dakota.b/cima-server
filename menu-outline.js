/*1. place different menus in different html files
2. load new files based on chosen items
3. use scripts to store form responses
4. store a deque of history

> Present initial options
< User clicks one
> Queue options and load the content object with results of http request from data-submenu
  (If data-submenu is not set, submit the form)
< User repeats with new item

Consider: "Back" button, "Start Over" button with prompt

Database Specification:
|######################|
|       menus          |
|######################|
| id               int |
| name         varchar |
|######################|
| route:      /menu/id |
| list:     /menu/list |
|######################|

output:
    route:
        SELECT * FROM menus WHERE id=?
        SELECT * FROM options WHERE parent=?
        */ var as = {
            "id":1,
            "handle":"s-1",
            "name":"Is this a safety issue?",
            "vertical":false,
            "options":[
                {
                    "id":1,
                    "handle":1,
                    "name":"Yes",
                    "parent":1,
                    "submenu":2
                },
                {
                    "id":2,
                    "handle":2,
                    "name":"No",
                    "parent":1,
                    "submenu":3
                }
            ]
        };
    /*list:
        SELECT * FROM menus

|######################|
|       options        |
|######################|
| id               int |
| value        varchar |
| handle           int |
| parent          menu |
| submenu         menu |
|######################|
| route:    /option/id |
| list:   /option/list |
|######################|

output:
    route:*/