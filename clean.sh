#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo Removing old images...
docker rmi -f dakota/research
echo Building image...
docker build .
echo Renaming image...
echo docker tag $(docker images -qf dangling=true)[0] dakota/research:latest
echo Cleaning...
docker rmi -f $(docker images -qf dangling=true)
echo Starting the image...
./run.sh
